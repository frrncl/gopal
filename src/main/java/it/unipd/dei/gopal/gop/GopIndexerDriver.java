/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.gop;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Callable;

/**
 * Indexes a Grid-of-Points (GoP) by using parallel {@link IndexerWorker}s.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class GopIndexerDriver extends GopDriver {

    /**
     * Creates a new {@code GopIndexerDriver}, reading its configuration from the given file.
     *
     * @param configFileName the name of the file containing the configuration for this {@code GopIndexerDriver}.
     */
    public GopIndexerDriver(final String configFileName) {
        super(configFileName);
    }

    /**
     * Creates a new {@code GopIndexerDriver}, using the default configuration file.
     */
    public GopIndexerDriver() {
        super();
    }

    @Override
    protected final int getMaxWorkers() {
        return indexWorkers;
    }

    @Override
    protected final Queue<Callable<Worker.Info>> getWorkers() {

        final Queue<Callable<Worker.Info>> queue = new ArrayDeque<>(runs.size());

        indexes.forEach(indexID -> queue
                .add(new IndexerWorker(indexID, analyzers.get(indexID), similarities.get(indexID), ramBufferSizeMB,
                                       indexPath, docsFile, expectedDocs)));

        return queue;
    }

    /**
     * Indexes a whole Grid-of-Points (GoP) using multiple parallel workers.
     *
     * @param args command-line arguments.
     */
    public static void main(String[] args) {

        GopIndexerDriver gid = null;
        if (args.length == 0) {
            gid = new GopIndexerDriver();
        } else {
            gid = new GopIndexerDriver(args[0]);
        }

        gid.executeWorkers();

        LOGGER.info("Execution terminated.");

    }

}
