/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.gop;

import it.unipd.dei.gopal.analysis.GopAnalyzer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.WordlistLoader;
import org.apache.lucene.search.similarities.Similarity;

import java.io.*;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Provides the common abstract class for indexing and searching a GoP by means of parallel {@link Worker}s.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class GopDriver {

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(GopDriver.class);

    /**
     * Name of the default configuration file
     */
    private static final String DEFAULT_CONFIG_FILE = "gopal.properties";

    /**
     * Prefix of all the properties
     */
    private static final String PROP_PREFIX = "it.unipd.dei.gopal";

    /**
     * Name of the property defining the base directory where indexes are stored
     */
    private static final String PROP_INDEX_PATH = PROP_PREFIX + ".index.path";

    /**
     * Name of the property defining the size in megabytes of the RAM buffer to be used for indexing
     */
    private static final String PROP_INDEX_RAM_BUFFER = PROP_PREFIX + ".index.ramBuffer";

    /**
     * Name of the property defining the number of parallel workers to be used for indexing
     */
    private static final String PROP_INDEX_WORKERS = PROP_PREFIX + ".index.workers";

    /**
     * Name of the property defining the number of parallel workers to be used for searching
     */
    private static final String PROP_SEARCH_WORKERS = PROP_PREFIX + ".search.workers";

    /**
     * Name of the property defining the file where the corpus is stored
     */
    private static final String PROP_CORPUS_FILE = PROP_PREFIX + ".corpus.file";

    /**
     * Name of the property defining the expected number of documents in the corpus
     */
    private static final String PROP_CORPUS_SIZE = PROP_PREFIX + ".corpus.size";

    /**
     * Name of the property defining the prefix for the run identifiers
     */
    private static final String PROP_RUN_PREFIX = PROP_PREFIX + ".run.prefix";

    /**
     * Name of the property defining the topic field to be used in the run identifier
     */
    private static final String PROP_RUN_TOPIC_FIELD = PROP_PREFIX + ".run.topicField";

    /**
     * Name of the property defining the base directory where runs are stored
     */
    private static final String PROP_RUN_PATH = PROP_PREFIX + ".run.path";

    /**
     * Name of the property defining the maximum number of documents to retrieve
     */
    private static final String PROP_RUN_MAX_DOCS_RETRIEVED = PROP_PREFIX + ".run.maxDocsRetrieved";

    /**
     * Name of the property defining the file where the topics are stored
     */
    private static final String PROP_TOPIC_FILE = PROP_PREFIX + ".topic.file";

    /**
     * Name of the property defining the expected number of topics
     */
    private static final String PROP_TOPIC_SIZE = PROP_PREFIX + ".topic.size";

    /**
     * Prefix of the properties defining stop lists
     */
    private static final String PROP_STOP_PREFIX = PROP_PREFIX + ".stopList";

    /**
     * Prefix of the properties defining stemmers
     */
    private static final String PROP_STEM_PREFIX = PROP_PREFIX + ".stemmer";

    /**
     * Prefix of the properties defining IR models
     */
    private static final String PROP_MODEL_PREFIX = PROP_PREFIX + ".model";

    /**
     * Suffix of the property defining labels of other properties
     */
    private static final String PROP_LABELS_SUFFIX = ".labels";

    /**
     * Suffix of the property defining description of other properties
     */
    private static final String PROP_DESCRIPTION_SUFFIX = ".description";

    /**
     * Suffix of the property defining class of other properties
     */
    private static final String PROP_CLASS_SUFFIX = ".class";

    /**
     * Suffix of the property defining whether to skip other properties
     */
    private static final String PROP_SKIP_SUFFIX = ".skip";

    /**
     * Suffix of the property defining the stop list file of other properties
     */
    private static final String PROP_STOPFILE_SUFFIX = ".stopFile";

    /**
     * The list of indexes in the GoP
     */
    protected final List<String> indexes = new ArrayList<>();

    /**
     * The list of runs in the GoP
     */
    protected final List<String> runs = new ArrayList<>();

    /**
     * The map of each run in the GoP to its analyzer
     */
    protected final Map<String, GopAnalyzer> analyzers = new HashMap<>();

    /**
     * The map of each run in the GoP to its similarity
     */
    protected final Map<String, Similarity> similarities = new HashMap<>();

    /**
     * The base directory where indexes are stored
     */
    protected final String indexPath;

    /**
     * The size in megabytes of the RAM buffer to be used for indexing
     */
    protected final int ramBufferSizeMB;

    /**
     * The number of parallel workers to be used for indexing
     */
    protected final int indexWorkers;

    /**
     * The number of parallel workers to be used for searching
     */
    protected final int searchWorkers;

    /**
     * The file where the corpus is stored
     */
    protected final String docsFile;

    /**
     * The expected number of documents in the corpus
     */
    final long expectedDocs;

    /**
     * The base directory where runs are stored
     */
    protected final String runPath;

    /**
     * The file where the topics are stored
     */
    protected final String topicsFile;

    /**
     * The expected number of topics
     */
    protected final int expectedTopics;

    /**
     * The prefix to be used in the run identifier
     */
    protected final String runPrefix;

    /**
     * The topic fields to be used in the run identifier
     */
    protected final String runTopicField;

    /**
     * The maximum number of documents to retrieve
     */
    protected final int maxDocsRetrieved;

    /**
     * The start time of the process
     */
    protected final long startTime;


    /**
     * The configuration for the GopDriver.
     */
    private final Map<String, String> config;


    /**
     * Creates a new {@code GopDriver}, reading its configuration from the given file.
     *
     * @param configFileName the name of the file containing the configuration for this {@code GopDriver}.
     */
    protected GopDriver(final String configFileName) {

        if (configFileName == null) {
            LOGGER.error("Configuration file name cannot be null.");
            throw new NullPointerException("Configuration file name cannot be null.");
        }

        if (configFileName.isEmpty()) {
            LOGGER.error("Configuration file name cannot be empty.");
            throw new IllegalArgumentException("Configuration file name cannot be empty.");
        }

        // Get the class loader
        ClassLoader cl = GopDriver.class.getClassLoader();
        if (cl == null) {
            cl = ClassLoader.getSystemClassLoader();
            LOGGER.debug("Using system class loader.");
        }

        // Get the stream for reading the configuration file
        final InputStream is = cl.getResourceAsStream(configFileName);
        if (is == null) {
            final Message msg = new ParameterizedMessage("The configuration file {} cannot be opened.", configFileName);
            LOGGER.error(msg);
            throw new IllegalStateException(msg.getFormattedMessage());
        }

        // Read the file
        final Properties cfg = new Properties();
        try {
            cfg.load(is);
        } catch (final IOException ioe) {
            final Message msg = new ParameterizedMessage("The configuration file {} cannot be loaded.", configFileName);
            LOGGER.error(msg, ioe);
            throw new IllegalStateException(msg.getFormattedMessage(), ioe);
        } finally {
            try {
                is.close();
            } catch (final IOException ioee) {
                final Message msg = new ParameterizedMessage(
                        "Unable to close the input stream for the configuration file {}.", configFileName);
                LOGGER.warn(msg, ioee);
            }
        }

        this.config = cfg.entrySet().stream().collect(
                Collectors.toMap(e -> e.getKey().toString(), e -> e.getValue().toString()));

        indexPath = config.get(PROP_INDEX_PATH);

        ramBufferSizeMB = Integer.parseInt(config.get(PROP_INDEX_RAM_BUFFER));

        indexWorkers = Integer.parseInt(config.get(PROP_INDEX_WORKERS));

        searchWorkers = Integer.parseInt(config.get(PROP_SEARCH_WORKERS));

        docsFile = config.get(PROP_CORPUS_FILE);

        expectedDocs = Long.parseLong(config.get(PROP_CORPUS_SIZE));

        runPath = config.get(PROP_RUN_PATH);

        topicsFile = config.get(PROP_TOPIC_FILE);

        expectedTopics = Integer.parseInt(config.get(PROP_TOPIC_SIZE));

        runPrefix = config.get(PROP_RUN_PREFIX);

        runTopicField = config.get(PROP_RUN_TOPIC_FIELD);

        maxDocsRetrieved = Integer.parseInt(config.get(PROP_RUN_MAX_DOCS_RETRIEVED));

        setup();

        startTime = System.currentTimeMillis();
    }

    /**
     * Creates a new {@code GopDriver}, using the default configuration file.
     */
    protected GopDriver() {
        this(DEFAULT_CONFIG_FILE);
    }


    /**
     * Creates and runs the given {@link Worker} in order to perform a test.
     */
    public final void executeWorkers() {

        final int maxWorkers = getMaxWorkers();
        int currentlyRun = 0;

        final ExecutorService es = Executors.newFixedThreadPool(maxWorkers);

        final ExecutorCompletionService<Worker.Info> ecs = new ExecutorCompletionService<Worker.Info>(es);

        final Queue<Callable<Worker.Info>> workers = getWorkers();

        final List<Worker.Info> completed = new ArrayList<>(runs.size());

        Worker w = null;
        Worker.Info wi = null;

        // repeat until there are still workers to submit (workers.size > 0)
        // or submitted workers in progress (currentlyRun > 0)
        while (workers.size() > 0 || currentlyRun > 0) {

            // while there are more workers, start them at batches of maxWorkers
            while (workers.size() > 0 && currentlyRun < maxWorkers) {

                w = (Worker) workers.poll();

                LOGGER.info("Starting worker {}, {} out of {}; {} worker(s) still to run.", w.info.workerID,
                            runs.size() - workers.size(), runs.size(), workers.size());

                ecs.submit(w);
                currentlyRun++;
            }

            try {
                wi = ecs.take().get();

                completed.add(wi);

                LOGGER.info("Worker {} completed in {}.", wi.workerID, millis2Hms(wi.elapsedTime));

                currentlyRun--;
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.warn(e);
            }

            LOGGER.info(" {} worker(s) left to run; {} worker(s) currently run.", workers.size(), currentlyRun);

        }

        if (completed.size() != runs.size()) {
            LOGGER.warn("{} worker(s) completed but {} worker(s) were expected to complete.", completed.size(),
                        runs.size());
        }

        List<Worker.Info> failed = completed.stream().filter(info -> !info.success).collect(Collectors.toList());

        long avgElapsedTime = (long) completed.stream().mapToLong(info -> info.elapsedTime).average().getAsDouble();

        LOGGER.info("{} out of {} worker(s) successfully completed.", runs.size() - failed.size(), runs.size());
        LOGGER.info("{} out of {} worker(s) failed to complete.", failed.size(), runs.size());
        LOGGER.info("Average elapsed time {}.", millis2Hms(avgElapsedTime));
        LOGGER.info("Total elapsed time {}.", millis2Hms(System.currentTimeMillis() - startTime));

        failed.forEach(info -> LOGGER.info("Worker {} failed; cause {}: {}", info.workerID,
                                           info.throwable != null ? info.throwable.getClass().getSimpleName() : null,
                                           info.throwable != null ? info.throwable.getMessage() : null));

        es.shutdownNow();

        LOGGER.info("Processing completed.");

    }

    /**
     * Returns the maximum number of parallel {@code Worker}s to be used.
     *
     * @return the maximum number of parallel {@code Worker}s to be used.
     */
    protected abstract int getMaxWorkers();

    /**
     * Returns the queue of {@code Workers} to be processed.
     *
     * @return the queue of {@code Workers} to be processed.
     */
    protected abstract Queue<Callable<Worker.Info>> getWorkers();


    /**
     * Provides a textual representation of the configuration of the {@code GopDriver}.
     */
    public String toString() {
        final ToStringBuilder tsb = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);

        final StringBuffer sb = tsb.getStringBuffer();

        // print each configuration parameter
        tsb.append("config");
        config.forEach((k, v) -> {
            sb.append(" ");
            tsb.append(k, v);
        });

        return tsb.toString();
    }


    /**
     * This auxiliary method converts a {@code long} value into a hh:mm:ss format.
     *
     * @param millis the value expressing a number of milliseconds.
     *
     * @return a string in the format hh:mm:ss.
     */
    private static final String millis2Hms(final long millis) {

        final NumberFormat nf = NumberFormat.getInstance();
        nf.setMinimumIntegerDigits(2);

        final int sec = (int) (millis / 1000);

        final int seconds = sec % 60;
        final int minutes = ((sec - seconds) / 60) % 60;
        final int hours = (sec - minutes * 60 - seconds) / 3600;

        return nf.format(hours) + ":" + nf.format(minutes) + ":" + nf.format(seconds) + "." + nf.format(millis % 1000);
    }

    /**
     * Parses a property containing a list of items into a list of strings.
     *
     * @param list the value of the property to parse.
     *
     * @return a list of strings.
     */
    private static List<String> parseList(final String list) {
        return Arrays.stream(list.split(",")).map(String::trim).collect(Collectors.toList());
    }


    /**
     * Sets up the Grid-of-Points (Gop)
     */
    private final void setup() {

        final List<String> stopLists = parseList(config.get(PROP_STOP_PREFIX + PROP_LABELS_SUFFIX));
        final List<String> stemmers = parseList(config.get(PROP_STEM_PREFIX + PROP_LABELS_SUFFIX));
        final List<String> models = parseList(config.get(PROP_MODEL_PREFIX + PROP_LABELS_SUFFIX));

        final Map<String, CharArraySet> stopMap = new HashMap<>(stopLists.size());
        final Map<String, Class<? extends TokenFilter>> stemMap = new HashMap<>(stemmers.size());
        final Map<String, Class<? extends Similarity>> modelMap = new HashMap<>(models.size());

        final ClassLoader cl = GopDriver.class.getClassLoader();


        // load stop lists
        stopLists.forEach(s -> {

            // if we have to use this stop list
            if (!Boolean.parseBoolean(config.get(PROP_STOP_PREFIX + "." + s + PROP_SKIP_SUFFIX))) {

                try {
                    Reader in = new BufferedReader(new InputStreamReader(
                            cl.getResourceAsStream(config.get(PROP_STOP_PREFIX + "." + s + PROP_STOPFILE_SUFFIX))));


                    stopMap.put(s, WordlistLoader.getWordSet(in));

                    in.close();

                } catch (IOException e) {
                    final Message msg = new ParameterizedMessage("Unable to load stop lists: {}", e.getMessage());
                    LOGGER.error(msg, e);
                    throw new IllegalStateException(msg.getFormattedMessage(), e);
                }
            }

        });

        stemmers.forEach(s -> {

            // if we have to use this stemmer
            if (!Boolean.parseBoolean(config.get(PROP_STEM_PREFIX + "." + s + PROP_SKIP_SUFFIX))) {
                try {
                    stemMap.put(s, Class.forName(config.get(PROP_STEM_PREFIX + "." + s + PROP_CLASS_SUFFIX))
                                        .asSubclass(TokenFilter.class));

                } catch (Exception e) {
                    final Message msg = new ParameterizedMessage("Unable to load stemmers: {}", e.getMessage());
                    LOGGER.error(msg, e);
                    throw new IllegalStateException(msg.getFormattedMessage(), e);
                }
            }

        });

        models.forEach(s -> {
            try {

                modelMap.put(s, Class.forName(config.get(PROP_MODEL_PREFIX + "." + s + PROP_CLASS_SUFFIX))
                                     .asSubclass(Similarity.class));

            } catch (Exception e) {
                final Message msg = new ParameterizedMessage("Unable to load IR models: {}", e.getMessage());
                LOGGER.error(msg, e);
                throw new IllegalStateException(msg.getFormattedMessage(), e);
            }
        });

        String indexID;
        String runID;
        boolean skipStop;
        boolean skipStem;
        GopAnalyzer ga;
        for (String stop : stopLists) {

            skipStop = Boolean.parseBoolean(config.get(PROP_STOP_PREFIX + "." + stop + PROP_SKIP_SUFFIX));

            for (String stem : stemmers) {

                skipStem = Boolean.parseBoolean(config.get(PROP_STEM_PREFIX + "." + stem + PROP_SKIP_SUFFIX));

                for (String model : models) {

                    indexID = createIndexID(stop, stem, model);
                    indexes.add(indexID);

                    runID = createRunID(stop, stem, model);
                    runs.add(runID);

                    if (skipStop) {
                        if (skipStem) {
                            ga = new GopAnalyzer();
                        } else {
                            ga = new GopAnalyzer(stemMap.get(stem));
                        }
                    } else {
                        if (skipStem) {
                            ga = new GopAnalyzer(stopMap.get(stop));
                        } else {
                            ga = new GopAnalyzer(stopMap.get(stop), stemMap.get(stem));
                        }
                    }

                    analyzers.put(indexID, ga);


                    try {

                        similarities.put(indexID, modelMap.get(model).newInstance());

                    } catch (Exception e) {
                        final Message msg = new ParameterizedMessage("Unable to instantiate IR models: {}",
                                                                     e.getMessage());
                        LOGGER.error(msg, e);
                        throw new IllegalStateException(msg.getFormattedMessage(), e);
                    }


                } // model
            } // stemmer
        } // stop


    }


    /**
     * Creates an index identifier.
     *
     * @param stop  the stop list to be used.
     * @param stem  the stemmer to be used.
     * @param model the IR model to be used.
     *
     * @return the index identifier.
     */
    private final String createIndexID(final String stop, final String stem, final String model) {
        return String.format("%s_%s_%s", stop, stem, model);
    }

    /**
     * Creates a run identifier.
     *
     * @param stop  the stop list to be used.
     * @param stem  the stemmer to be used.
     * @param model the IR model to be used.
     *
     * @return the run identifier.
     */
    private final String createRunID(final String stop, final String stem, final String model) {
        return String.format("%s_%s_%s_%s_%s", runPrefix, stop, stem, model, runTopicField);
    }

}
