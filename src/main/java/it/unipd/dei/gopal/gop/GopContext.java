/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.gop;

import org.apache.logging.log4j.ThreadContext;


/**
 * Provides the overall context of the application on a per-thread basis.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public final class GopContext {

    /**
     * The worker currently processed
     */
    private static final String WORKER = "WORKER";


    /**
     * Sets the {@code Worker} currently processed.
     *
     * If {@code null}, it simply returns.
     *
     * @param workerID the worker currently processed.
     */
    public static void setWorker(final String workerID) {
        if (workerID != null) {
            ThreadContext.put(WORKER, workerID);
        }
    }

    /**
     * Returns the worker currently processed.
     *
     * @return the worker currently processed, if any; {@code null} otherwise.
     */
    public static String getWorker() {
        return ThreadContext.get(WORKER);
    }


    /**
     * Removes the worker currently processed.
     */
    public static void removeWorker() {
        ThreadContext.remove(WORKER);
    }


    /**
     * This class can be neither instantiated nor sub-classed.
     */
    private GopContext() {
        throw new AssertionError(String.format("No instances of %s allowed.", GopContext.class.getName()));
    }
}
