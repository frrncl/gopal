/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.gop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.util.concurrent.Callable;

/**
 * Represents the common abstract class for indexing or searching a corpus.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class Worker implements Callable<Worker.Info> {

    /**
     * Represents information about the given {@code Worker}.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    public final class Info {

        /**
         * The identifier of the {@code Worker}.
         */
        public final String workerID;

        /**
         * The exit status of a {@code Worker}: {@code true} if the processing completed without errors, {@code false}
         * otherwise.
         */
        public boolean success = true;

        /**
         * The total processing time of the {@code Worker}.
         */
        public long elapsedTime = Long.MIN_VALUE;

        /**
         * Any {@code Throwable} raised during the execution of the {@code Worker}.
         */
        public Throwable throwable = null;

        /**
         * Creates a new information pack for the given run.
         *
         * @param workerID the identifier of the worker.
         */
        public Info(final String workerID) {
            this.workerID = workerID;
        }
    }

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(Worker.class);

    /**
     * The information on this worker.
     */
    protected final Info info;

    /**
     * Creates a new worker.
     *
     * @param workerID the identifier of this worker.
     */
    public Worker(final String workerID) {

        if (workerID == null) {
            LOGGER.error("Worker identifier cannot be null.");
            throw new NullPointerException("Worker identifier cannot be null.");
        }

        if (workerID.isEmpty()) {
            LOGGER.error("Worker identifier cannot be empty.");
            throw new IllegalArgumentException("Worker identifier cannot be empty.");
        }

        this.info = new Info(workerID);
    }

    /**
     * Processes a collection.
     *
     * @return an exception raised during processing, if any; {@code null} otherwise.
     */
    @Override
    public Worker.Info call() {

        try {
            process();
        } catch (Exception e) {
            final Message msg = new ParameterizedMessage("Unable to perform worker {}: {}", info.workerID,
                                                         e.getMessage());
            LOGGER.error(msg, e);

            info.success = false;
            info.throwable = e;
        } finally {
            return info;
        }
    }

    /**
     * Performs the actual processing.
     *
     * @throws Exception if something goes wrong while processing.
     */
    protected abstract void process() throws Exception;

}
