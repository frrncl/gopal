/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.gop;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Callable;

/**
 * Searches a Grid-of-Points (GoP) by using parallel {@link SearcherWorker}s.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class GopSearcherDriver extends GopDriver {

    /**
     * Creates a new {@code GopSearcherDriver}, reading its configuration from the given file.
     *
     * @param configFileName the name of the file containing the configuration for this {@code GopIndexerDriver}.
     */
    public GopSearcherDriver(final String configFileName) {
        super(configFileName);
    }

    /**
     * Creates a new {@code GopSearcherDriver}, using the default configuration file.
     */
    public GopSearcherDriver() {
        super();
    }

    @Override
    protected final int getMaxWorkers() {
        return searchWorkers;
    }

    @Override
    protected final Queue<Callable<Worker.Info>> getWorkers() {

        final Queue<Callable<Worker.Info>> queue = new ArrayDeque<Callable<Worker.Info>>(runs.size());

        String indexID = null;
        for (int i = 0, n = indexes.size(); i < n; i++) {
            indexID = indexes.get(i);
            queue.add(new SearcherWorker(runs.get(i), analyzers.get(indexID), similarities.get(indexID), indexPath,
                                         indexID, topicsFile, expectedTopics, runPath, maxDocsRetrieved));
        }

        return queue;
    }

    /**
     * Indexes a whole Grid-of-Points (GoP) using multiple parallel workers.
     *
     * @param args command-line arguments.
     */
    public static void main(String[] args) {

        GopSearcherDriver gsd = null;
        if (args.length == 0) {
            gsd = new GopSearcherDriver();
        } else {
            gsd = new GopSearcherDriver(args[0]);
        }

        gsd.executeWorkers();

        LOGGER.info("Execution terminated.");

    }

}
