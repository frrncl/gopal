/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.gop;

import it.unipd.dei.gopal.search.Searcher;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.Similarity;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Represents a worker for searching a corpus. It corresponds to a single point in a GoP.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class SearcherWorker extends Worker {

    /**
     * The searcher to be used
     */
    final Searcher searcher;

    /**
     * Creates a new indexer worker.
     *
     * @param runID            the identifier of the run processed by this worker.
     * @param analyzer         the {@code Analyzer} to be used.
     * @param similarity       the {@code Similarity} to be used.
     * @param indexPath        the directory containing the index to be searched.
     * @param indexID          the identifier of the index processed by this worker.
     * @param topicsFile       the file containing the topics to search for.
     * @param expectedTopics   the total number of topics expected to be searched.
     * @param runPath          the path where to store the run.
     * @param maxDocsRetrieved the maximum number of documents to be retrieved.
     *
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public SearcherWorker(final String runID, final Analyzer analyzer, final Similarity similarity,
                          final String indexPath, final String indexID, final String topicsFile,
                          final int expectedTopics, final String runPath, final int maxDocsRetrieved) {

        super(runID);

        if (indexPath == null) {
            LOGGER.error("Index path cannot be null.");
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            LOGGER.error("Index path cannot be empty.");
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isWritable(indexDir)) {
            Message msg = new ParameterizedMessage("Index path {} cannot be written.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(indexDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory where to write the index.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (indexID == null) {
            LOGGER.error("Index identifier cannot be null.");
            throw new NullPointerException("Index identifier cannot be null.");
        }

        if (indexID.isEmpty()) {
            LOGGER.error("Index identifier cannot be empty.");
            throw new IllegalArgumentException("Index identifier cannot be empty.");
        }

        Path runIndexPath = indexDir.resolve(indexID);

        searcher = new Searcher(analyzer, similarity, runIndexPath.toAbsolutePath().toString(), topicsFile,
                                expectedTopics, runID, runPath, maxDocsRetrieved);

    }

    @Override
    protected final void process() throws Exception {
        GopContext.setWorker(info.workerID);

        try {
            searcher.search();
        } finally {
            info.elapsedTime = searcher.getElapsedTime();
            GopContext.removeWorker();
        }
    }

}
