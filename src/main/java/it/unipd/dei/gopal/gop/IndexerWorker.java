/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.gop;

import it.unipd.dei.gopal.index.SingleFileIndexer;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.similarities.Similarity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Represents a worker for indexing a corpus. It corresponds to a single point in a GoP.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class IndexerWorker extends Worker {

    /**
     * The indexer to be used
     */
    final SingleFileIndexer indexer;

    /**
     * Creates a new indexer worker.
     *
     * @param workerID        the identifier of the worker.
     * @param analyzer        the {@code Analyzer} to be used.
     * @param similarity      the {@code Similarity} to be used.
     * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
     * @param indexPath       the directory where to store the index.
     * @param docsFile        the file from which documents have to be read.
     * @param expectedDocs    the total number of documents expected to be indexed
     *
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public IndexerWorker(final String workerID, final Analyzer analyzer, final Similarity similarity,
                         final int ramBufferSizeMB, final String indexPath, final String docsFile,
                         final long expectedDocs) {

        super(workerID);

        if (indexPath == null) {
            LOGGER.error("Index path cannot be null.");
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            LOGGER.error("Index path cannot be empty.");
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isWritable(indexDir)) {
            Message msg = new ParameterizedMessage("Index path {} cannot be written.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(indexDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory where to write the index.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        Path runPath = indexDir.resolve(workerID);

        if (!Files.exists(runPath)) {
            try {
                Files.createDirectories(runPath);
            } catch (IOException e) {
                Message msg = new ParameterizedMessage("Run index directory {} cannot be created.",
                                                       runPath.toAbsolutePath().toString());
                LOGGER.error(msg, e);
                throw new IllegalArgumentException(msg.getFormattedMessage(), e);
            }
        }

        indexer = new SingleFileIndexer(analyzer, similarity, ramBufferSizeMB, runPath.toAbsolutePath().toString(),
                                        docsFile, expectedDocs);

    }

    @Override
    protected final void process() throws Exception {
        GopContext.setWorker(info.workerID);

        try {
            indexer.index();
        } finally {
            info.elapsedTime = indexer.getElapsedTime();
            GopContext.removeWorker();
        }
    }

}
