/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.index;

import it.unipd.dei.gopal.parser.ParsedDocument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Indexes documents processing a single text file where each line is a document.
 *
 * In each line, the first field is the document identifier and the next field is the whole document body.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class SingleFileIndexer {

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(SingleFileIndexer.class);

    /**
     * The index writer.
     */
    private final IndexWriter writer;

    /**
     * The input file.
     */
    private final Reader in;

    /**
     * The total number of documents expected to be indexed.
     */
    private final long expectedDocs;

    /**
     * The total number of indexed documents.
     */
    private long docsCount;

    /**
     * The total elapsed time.
     */
    private long elapsedTime = Long.MIN_VALUE;

    /**
     * Creates a new indexer.
     *
     * @param analyzer        the {@code Analyzer} to be used.
     * @param similarity      the {@code Similarity} to be used.
     * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
     * @param indexPath       the directory where to store the index.
     * @param docsFile        the file from which documents have to be read.
     * @param expectedDocs    the total number of documents expected to be indexed
     *
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public SingleFileIndexer(final Analyzer analyzer, final Similarity similarity, final int ramBufferSizeMB,
                             final String indexPath, final String docsFile, final long expectedDocs) {

        if (analyzer == null) {
            LOGGER.error("Analyzer cannot be null.");
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            LOGGER.error("Similarity cannot be null.");
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            LOGGER.error("RAM buffer size cannot be less than or equal to zero.");
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);

        if (indexPath == null) {
            LOGGER.error("Index path cannot be null.");
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            LOGGER.error("Index path cannot be empty.");
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isWritable(indexDir)) {
            Message msg = new ParameterizedMessage("Index directory {} cannot be written.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(indexDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory where to write the index.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }


        if (docsFile == null) {
            LOGGER.error("Documents file cannot be null.");
            throw new NullPointerException("Documents file cannot be null.");
        }

        if (docsFile.isEmpty()) {
            LOGGER.error("Documents file cannot be empty.");
            throw new IllegalArgumentException("Documents file cannot be empty.");
        }

        try {
            in = Files.newBufferedReader(Paths.get(docsFile), StandardCharsets.UTF_8);
        } catch (IOException e) {
            Message msg = new ParameterizedMessage("Unable to open input file {}: {}.", docsFile, e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

        if (expectedDocs <= 0) {
            LOGGER.error("The expected number of documents to be indexed cannot be less than or equal to zero.");
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        try {
            writer = new IndexWriter(FSDirectory.open(indexDir), iwc);
        } catch (IOException e) {
            Message msg = new ParameterizedMessage("Unable to create the index writer in directory {}: {}.",
                                                   indexDir.toAbsolutePath().toString(), e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

    }

    /**
     * Returns the total elapsed time.
     *
     * @return the total elapsed time.
     */
    public long getElapsedTime() {
        return elapsedTime;
    }

    /**
     * Indexes the documents.
     *
     * @throws IOException if something goes wrong while indexing.
     */
    public void index() throws IOException {


        // the start time of the indexing
        final long start = System.currentTimeMillis();

        LOGGER.info("Start indexing.");

        Scanner s = new Scanner(in);
        s.useDelimiter("\t");

        Document doc = null;

        try {
            while (s.hasNextLine()) {
                doc = new Document();

                // add the document identifier
                doc.add(new StringField(ParsedDocument.FIELDS.ID, s.next(), Field.Store.YES));

                // add the document body
                doc.add(new BodyField(s.nextLine()));

                writer.addDocument(doc);

                docsCount++;

                if (docsCount % 100000 == 0) {
                    elapsedTime = System.currentTimeMillis() - start;

                    LOGGER.info("{} document(s) indexed in {} seconds.", docsCount, elapsedTime / 1000);
                }
            }

            writer.commit();

            writer.close();

            s.close();

        } catch (RuntimeException e) { // just for extra logging in case of errors while indexing
            Message msg = new ParameterizedMessage("Exception while indexing line {} (maybe +1), docid {}.", docsCount,
                                                   doc.getValues(ParsedDocument.FIELDS.ID)[0].substring(0, Math.min(
                                                           doc.getValues(ParsedDocument.FIELDS.ID)[0].length(), 100)));
            LOGGER.error(msg, e);
            throw e;
        }

        if (docsCount != expectedDocs) {
            LOGGER.warn("Expected to index {} documents; {} indexed instead.", expectedDocs, docsCount);
        }

        elapsedTime = System.currentTimeMillis() - start;

        LOGGER.info("{} document(s) indexed in {} seconds.", docsCount, elapsedTime / 1000);

    }


    public static void main(String[] args) throws Exception {

        final int ramBuffer = 128;
        final String docsFile = "/Users/ferro/Documents/pubblicazioni/2019/SIGIR2019/FF/gopal/corpus/wapo.txt";
        final String indexPath = "/Users/ferro/Documents/pubblicazioni/2019/SIGIR2019/FF/gopal/tmp";

        final int expectedDocs = 595037;

        SingleFileIndexer i = new SingleFileIndexer(new StandardAnalyzer(), new ClassicSimilarity(), ramBuffer,
                                                    indexPath, docsFile, expectedDocs);

        i.index();


    }

}
