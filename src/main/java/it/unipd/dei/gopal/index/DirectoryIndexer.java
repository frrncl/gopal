/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.index;

import it.unipd.dei.gopal.parser.DocumentParser;
import it.unipd.dei.gopal.parser.NYTParser;
import it.unipd.dei.gopal.parser.ParsedDocument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Indexes documents processing a whole directory tree.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class DirectoryIndexer {

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(DirectoryIndexer.class);

    /**
     * The index writer.
     */
    private final IndexWriter writer;

    /**
     * The class of the {@code DocumentParser} to be used.
     */
    private final Class<? extends DocumentParser> dpCls;

    /**
     * The directory (and sub-directories) where documents are stored.
     */
    private final Path docsDir;

    /**
     * The extension of the files to be indexed.
     */
    private final String extension;

    /**
     * The charset used for encoding documents.
     */
    private final Charset cs;

    /**
     * The total number of documents expected to be indexed.
     */
    private final long expectedDocs;

    /**
     * The start instant of the indexing.
     */
    private final long start;

    /**
     * The total number of indexed documents.
     */
    private long docsCount;

    /**
     * The total number of indexed bytes
     */
    private long bytesCount;

    /**
     * Creates a new indexer.
     *
     * @param analyzer        the {@code Analyzer} to be used.
     * @param similarity      the {@code Similarity} to be used.
     * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
     * @param indexPath       the directory where to store the index.
     * @param docsPath        the directory from which documents have to be read.
     * @param extension       the extension of the files to be indexed.
     * @param charsetName     the name of the charset used for encoding documents.
     * @param expectedDocs    the total number of documents expected to be indexed
     * @param dpCls           the class of the {@code DocumentParser} to be used.
     *
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public DirectoryIndexer(final Analyzer analyzer, final Similarity similarity, final int ramBufferSizeMB,
                            final String indexPath, final String docsPath, final String extension,
                            final String charsetName, final long expectedDocs,
                            final Class<? extends DocumentParser> dpCls) {

        if (dpCls == null) {
            LOGGER.error("Document parser class cannot be null.");
            throw new NullPointerException("Document parser class cannot be null.");
        }

        this.dpCls = dpCls;

        if (analyzer == null) {
            LOGGER.error("Analyzer cannot be null.");
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            LOGGER.error("Similarity cannot be null.");
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (ramBufferSizeMB <= 0) {
            LOGGER.error("RAM buffer size cannot be less than or equal to zero.");
            throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
        }

        final IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setSimilarity(similarity);
        iwc.setRAMBufferSizeMB(ramBufferSizeMB);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        iwc.setCommitOnClose(true);

        if (indexPath == null) {
            LOGGER.error("Index path cannot be null.");
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            LOGGER.error("Index path cannot be empty.");
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isWritable(indexDir)) {
            Message msg = new ParameterizedMessage("Index directory {} cannot be written.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(indexDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory where to write the index.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }


        if (docsPath == null) {
            LOGGER.error("Documents path cannot be null.");
            throw new NullPointerException("Documents path cannot be null.");
        }

        if (docsPath.isEmpty()) {
            LOGGER.error("Documents path cannot be empty.");
            throw new IllegalArgumentException("Documents path cannot be empty.");
        }

        final Path docsDir = Paths.get(docsPath);
        if (!Files.isReadable(docsDir)) {
            Message msg = new ParameterizedMessage("Documents directory {} cannot be read.",
                                                   docsDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(docsDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory of documents.",
                                                   docsDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        this.docsDir = docsDir;

        if (extension == null) {
            LOGGER.error("File extension cannot be null.");
            throw new NullPointerException("File extension cannot be null.");
        }

        if (extension.isEmpty()) {
            LOGGER.error("File extension cannot be empty.");
            throw new IllegalArgumentException("File extension cannot be empty.");
        }
        this.extension = extension;

        if (charsetName == null) {
            LOGGER.error("Charset name cannot be null.");
            throw new NullPointerException("Charset name cannot be null.");
        }

        if (charsetName.isEmpty()) {
            LOGGER.error("Charset name cannot be empty.");
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }

        try {
            cs = Charset.forName(charsetName);
        } catch (Exception e) {
            Message msg = new ParameterizedMessage("Unable to create the charset {}: {}.", charsetName, e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

        if (expectedDocs <= 0) {
            LOGGER.error("The expected number of documents to be indexed cannot be less than or equal to zero.");
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        this.bytesCount = 0;

        try {
            writer = new IndexWriter(FSDirectory.open(indexDir), iwc);
        } catch (IOException e) {
            Message msg = new ParameterizedMessage("Unable to create the index writer in directory {}: {}.",
                                                   indexDir.toAbsolutePath().toString(), e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

        this.start = System.currentTimeMillis();

    }

    /**
     * Indexes the documents.
     *
     * @throws IOException if something goes wrong while indexing.
     */
    public void index() throws IOException {

        LOGGER.info("Start indexing.");

        Files.walkFileTree(docsDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.getFileName().toString().endsWith(extension)) {

                    DocumentParser dp = DocumentParser.create(dpCls, Files.newBufferedReader(file, cs));

                    Document doc = null;

                    for (ParsedDocument pd : dp) {

                        doc = new Document();

                        // add the document identifier
                        doc.add(new StringField(ParsedDocument.FIELDS.ID, pd.getIdentifier(), Field.Store.YES));

                        // add the document body
                        doc.add(new BodyField(pd.getBody()));

                        writer.addDocument(doc);

                        docsCount++;
                        bytesCount += Files.size(file);

                        if (docsCount % 1000 == 0) {
                            LOGGER.info("{} document(s) ({} bytes) indexed in {}.", docsCount, bytesCount,
                                        System.currentTimeMillis() - start);
                        }

                    }

                }
                return FileVisitResult.CONTINUE;
            }
        });

        writer.commit();

        writer.close();

        if (docsCount != expectedDocs) {
            LOGGER.warn("Expected to index {} documents; {} indexed instead.", expectedDocs, docsCount);
        }

        LOGGER.info("{} document(s) ({} bytes) indexed in {}.", docsCount, bytesCount,
                    System.currentTimeMillis() - start);


    }


    public static void main(String[] args) throws Exception {

        final int ramBuffer = 128;
        final String docsPath = "/Users/ferro/Documents/experimental-collections/corpora/TREC/nyt_corpus/documents";
        final String indexPath = "/Users/ferro/Documents/pubblicazioni/2017/TREC/indexes/prova";

        final String extension = "xml";
        final int expectedDocs = 1855658;
        final String charsetName = "UTF-8";

        DirectoryIndexer i = new DirectoryIndexer(new StandardAnalyzer(), new ClassicSimilarity(), ramBuffer, indexPath,
                                                  docsPath, extension, charsetName, expectedDocs, NYTParser.class);

        i.index();


    }

}
