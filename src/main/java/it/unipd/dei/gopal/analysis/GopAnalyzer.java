/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.analysis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

import java.lang.reflect.Constructor;

/**
 * Provides the base analyzer for a GoP (Grid-of-Points), constituted by a {@link StandardTokenizer}, a {@link
 * EnglishPossessiveFilter}, a {@link org.apache.lucene.analysis.core.StopFilter} if any, and a stemmer, if any.
 * <p>
 * It is based on {@link org.apache.lucene.analysis.en.EnglishAnalyzer}, adapted to work with GoP, and focused on the
 * English language.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class GopAnalyzer extends StopwordAnalyzerBase {

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(GopAnalyzer.class);

    /**
     * Default maximum allowed token length
     */
    private static final int MAX_TOKEN_LENGTH = 255;

    /**
     * The stemmer to be used, if any
     */
    private final Constructor<? extends TokenFilter> stemmer;

    /**
     * Builds an analyzer without any stop list and stemmer.
     */
    public GopAnalyzer() {
        this(null, null);
    }

    /**
     * Builds an analyzer with the given stop list and no stemmer.
     *
     * @param stopList the stop list to be used, if any.
     */
    public GopAnalyzer(CharArraySet stopList) {
        this(stopList, null);
    }

    /**
     * Builds an analyzer with the given stemmer and no stop list.
     *
     * @param stemmer the stemmer to be used, if any.
     */
    public GopAnalyzer(Class<? extends TokenFilter> stemmer) {
        this(null, stemmer);
    }

    /**
     * Builds an analyzer with the given stop list and stemmer.
     *
     * @param stopList the stop list to be used, if any.
     * @param stemmer  the stemmer to be used, if any.
     */
    public GopAnalyzer(CharArraySet stopList, Class<? extends TokenFilter> stemmer) {
        super(stopList == null ? CharArraySet.EMPTY_SET : stopList);

        try {
            this.stemmer = stemmer != null ? stemmer.getConstructor(TokenStream.class) : null;
        } catch (Exception e) {
            Message msg = new ParameterizedMessage("Unable to get the constructor for the stemmer {}.",
                                                   stemmer.getName());
            LOGGER.error(msg, e);
            throw new IllegalStateException(msg.getFormattedMessage(), e);
        }
    }


    @Override
    protected TokenStreamComponents createComponents(final String fieldName) {
        final StandardTokenizer src = new StandardTokenizer();
        src.setMaxTokenLength(MAX_TOKEN_LENGTH);
        TokenStream tok = new EnglishPossessiveFilter(src);
        tok = new LowerCaseFilter(tok);
        tok = new StopFilter(tok, stopwords);

        if (stemmer != null) {
            try {
                tok = stemmer.newInstance(tok);
            } catch (Exception e) {
                Message msg = new ParameterizedMessage("Unable to instantiate the stemmer {}.",
                                                       stemmer.getDeclaringClass().getName());
                LOGGER.error(msg, e);
                throw new IllegalStateException(msg.getFormattedMessage(), e);
            }

        }

        return new TokenStreamComponents(r -> {
            src.setMaxTokenLength(MAX_TOKEN_LENGTH);
            src.setReader(r);
        }, tok);


    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }

}
