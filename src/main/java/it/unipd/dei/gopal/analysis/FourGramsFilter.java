/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.analysis;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;

import java.io.IOException;

/**
 * Computes 4-grams.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class FourGramsFilter extends TokenFilter {

    /**
     * The minimum length of a n-gram
     */
    private static final int MIN_GRAM = 4;

    /**
     * The maximum length of a n-gram
     */
    private static final int MAX_GRAM = 4;

    /**
     * Performs the actual processing.
     *
     * It is a pity that {@link NGramTokenFilter} is declared as a final class. This prevents you from sub-classing it
     * to have a parameter-free constructor; instead, you have to wrap it and loose some performance in useless method
     * forwarding.
     */
    private final NGramTokenFilter gram;


    /**
     * Creates a new n-grammer.
     *
     * @param input the input token stream to process.
     */
    public FourGramsFilter(TokenStream input) {
        super(input);
        gram = new NGramTokenFilter(input, MIN_GRAM, MAX_GRAM, false);
    }

    @Override
    public boolean incrementToken() throws IOException {
        return gram.incrementToken();
    }
}
