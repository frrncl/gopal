/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.search;

import it.unipd.dei.gopal.parser.ParsedDocument;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.benchmark.quality.QualityQuery;
import org.apache.lucene.benchmark.quality.trec.TrecTopicsReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParserBase;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.FSDirectory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Searches a document collection.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class Searcher {

    /**
     * The fields of the typical TREC topics.
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    private static final class TOPIC_FIELDS {

        /**
         * The title of a topic.
         */
        public static final String TITLE = "title";

        /**
         * The description of a topic.
         */
        public static final String DESCRIPTION = "description";

        /**
         * The narrative of a topic.
         */
        public static final String NARRATIVE = "narrative";
    }


    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(Searcher.class);

    /**
     * The identifier of the run
     */
    private final String runID;

    /**
     * The run to be written
     */
    private final PrintWriter run;

    /**
     * The index reader
     */
    private final IndexReader reader;

    /**
     * The index searcher.
     */
    private final IndexSearcher searcher;

    /**
     * The topics to be searched
     */
    private final QualityQuery[] topics;

    /**
     * The query parser
     */
    private final QueryParser qp;

    /**
     * The maximum number of documents to retrieve
     */
    private final int maxDocsRetrieved;

    /**
     * The total elapsed time.
     */
    private long elapsedTime = Long.MIN_VALUE;


    /**
     * Creates a new searcher.
     *
     * @param analyzer         the {@code Analyzer} to be used.
     * @param similarity       the {@code Similarity} to be used.
     * @param indexPath        the directory where containing the index to be searched.
     * @param topicsFile       the file containing the topics to search for.
     * @param expectedTopics   the total number of topics expected to be searched.
     * @param runID            the identifier of the run to be created.
     * @param runPath          the path where to store the run.
     * @param maxDocsRetrieved the maximum number of documents to be retrieved.
     *
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public Searcher(final Analyzer analyzer, final Similarity similarity, final String indexPath,
                    final String topicsFile, final int expectedTopics, final String runID, final String runPath,
                    final int maxDocsRetrieved) {

        if (analyzer == null) {
            LOGGER.error("Analyzer cannot be null.");
            throw new NullPointerException("Analyzer cannot be null.");
        }

        if (similarity == null) {
            LOGGER.error("Similarity cannot be null.");
            throw new NullPointerException("Similarity cannot be null.");
        }

        if (indexPath == null) {
            LOGGER.error("Index path cannot be null.");
            throw new NullPointerException("Index path cannot be null.");
        }

        if (indexPath.isEmpty()) {
            LOGGER.error("Index path cannot be empty.");
            throw new IllegalArgumentException("Index path cannot be empty.");
        }

        final Path indexDir = Paths.get(indexPath);
        if (!Files.isReadable(indexDir)) {
            Message msg = new ParameterizedMessage("Index directory {} cannot be read.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(indexDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory where to search the index.",
                                                   indexDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        try {
            reader = DirectoryReader.open(FSDirectory.open(indexDir));
        } catch (IOException e) {
            Message msg = new ParameterizedMessage("Unable to create the index reader for directory {}: {}.",
                                                   indexDir.toAbsolutePath().toString(), e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

        searcher = new IndexSearcher(reader);
        searcher.setSimilarity(similarity);

        if (topicsFile == null) {
            LOGGER.error("Topics file cannot be null.");
            throw new NullPointerException("Topics file cannot be null.");
        }

        if (topicsFile.isEmpty()) {
            LOGGER.error("Topics file cannot be empty.");
            throw new IllegalArgumentException("Topics file cannot be empty.");
        }

        try {
            BufferedReader in = Files.newBufferedReader(Paths.get(topicsFile), StandardCharsets.UTF_8);

            topics = new TrecTopicsReader().readQueries(in);

            in.close();
        } catch (IOException e) {
            Message msg = new ParameterizedMessage("Unable to process topic file {}: {}.", topicsFile, e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }


        if (expectedTopics <= 0) {
            LOGGER.error("The expected number of topics to be searched cannot be less than or equal to zero.");
            throw new IllegalArgumentException(
                    "The expected number of topics to be searched cannot be less than or equal to zero.");
        }

        if (topics.length != expectedTopics) {
            LOGGER.warn("Expected to search for {} topics; {} topics found instead.", expectedTopics, topics.length);
        }

        qp = new QueryParser(ParsedDocument.FIELDS.BODY, analyzer);


        if (runID == null) {
            LOGGER.error("Run identifier cannot be null.");
            throw new NullPointerException("Run identifier cannot be null.");
        }

        if (runID.isEmpty()) {
            LOGGER.error("Run identifier cannot be empty.");
            throw new IllegalArgumentException("Run identifier cannot be empty.");
        }

        this.runID = runID;


        if (runPath == null) {
            LOGGER.error("Run path cannot be null.");
            throw new NullPointerException("Run path cannot be null.");
        }

        if (runPath.isEmpty()) {
            LOGGER.error("Run path cannot be empty.");
            throw new IllegalArgumentException("Run path cannot be empty.");
        }

        final Path runDir = Paths.get(runPath);
        if (!Files.isWritable(runDir)) {
            Message msg = new ParameterizedMessage("Run directory {} cannot be written.",
                                                   runDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(runDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory where to write the run.",
                                                   runDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        Path runFile = runDir.resolve(runID + ".txt");
        try {
            run = new PrintWriter(Files.newBufferedWriter(runFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                                                          StandardOpenOption.TRUNCATE_EXISTING,
                                                          StandardOpenOption.WRITE));
        } catch (IOException e) {
            Message msg = new ParameterizedMessage("Unable to open run file {}: {}.", runFile.toAbsolutePath(),
                                                   e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

        if (maxDocsRetrieved <= 0) {
            LOGGER.error("The maximum number of documents to be retrieved cannot be less than or equal to zero.");
            throw new IllegalArgumentException(
                    "The maximum number of documents to be retrieved cannot be less than or equal to zero.");
        }

        this.maxDocsRetrieved = maxDocsRetrieved;
    }

    /**
     * Returns the total elapsed time.
     *
     * @return the total elapsed time.
     */
    public long getElapsedTime() {
        return elapsedTime;
    }

    /**
     * /** Searches for the specified topics.
     *
     * @throws IOException    if something goes wrong while searching.
     * @throws ParseException if something goes wrong while parsing topics.
     */
    public void search() throws IOException, ParseException {

        // the start time of the searching
        final long start = System.currentTimeMillis();

        final Set<String> idField = new HashSet<>();
        idField.add(ParsedDocument.FIELDS.ID);

        BooleanQuery.Builder bq = null;
        Query q = null;
        TopDocs docs = null;
        ScoreDoc[] sd = null;
        String docID = null;

        try {
            for (QualityQuery t : topics) {

                LOGGER.info("Searching for topic {}.", t.getQueryID());

                bq = new BooleanQuery.Builder();

                bq.add(qp.parse(QueryParserBase.escape(t.getValue(TOPIC_FIELDS.TITLE))), BooleanClause.Occur.SHOULD);
                bq.add(qp.parse(QueryParserBase.escape(t.getValue(TOPIC_FIELDS.DESCRIPTION))),
                       BooleanClause.Occur.SHOULD);

                q = bq.build();

                docs = searcher.search(q, maxDocsRetrieved);

                sd = docs.scoreDocs;

                for (int i = 0, n = sd.length; i < n; i++) {
                    docID = reader.document(sd[i].doc, idField).get(ParsedDocument.FIELDS.ID);

                    run.printf(Locale.ENGLISH, "%s\tQ0\t%s\t%d\t%.6f\t%s%n", t.getQueryID(), docID, i, sd[i].score,
                               runID);
                }

                run.flush();

            }
        } finally {
            run.close();

            reader.close();
        }

        elapsedTime = System.currentTimeMillis() - start;

        LOGGER.info("{} topic(s) searched in {} seconds.", topics.length, elapsedTime / 1000);

    }


    public static void main(String[] args) throws Exception {

        //String topics = "/Users/ferro/Documents/pubblicazioni/2017/TREC/topics/TREC13_robust.txt";
        // String topics = "/Users/ferro/Documents/pubblicazioni/2017/TREC/topics/core_crowd.txt";

        //final String indexPath = "/Users/ferro/Documents/pubblicazioni/2017/TREC/tipster/indexes/nostop_nostem_bm25";

        //final String runPath = "/Users/ferro/Documents/pubblicazioni/2017/TREC/tipster/runs";

        //final String runID = "ims-rob_nostop_nostem_bm25_td";

        //final int maxDocsRetrieved = 1000;

        //Searcher s = new Searcher(new GopAnalyzer(), new BM25Similarity(), indexPath, topics, 50, runID, runPath,
        //                          maxDocsRetrieved);

        //s.search();

        BufferedReader in = Files.newBufferedReader(Paths.get("/Users/ferro/Documents/pubblicazioni/2019/SIGIR2019/FF/gopal/topics/TREC27_core.txt"), StandardCharsets.UTF_8);

        QualityQuery[] topics = new TrecTopicsReader().readQueries(in);

        for(QualityQuery t : topics) {

            System.out.printf("%n%n### %s # title = %s # description = %s # narrative = %s %n", t.getQueryID(),
                              t.getValue(TOPIC_FIELDS.TITLE), t.getValue(TOPIC_FIELDS.DESCRIPTION), t.getValue(TOPIC_FIELDS.NARRATIVE)
                              );

        }

    }

}
