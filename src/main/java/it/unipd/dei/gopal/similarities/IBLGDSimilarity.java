/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.similarities;

import org.apache.lucene.search.similarities.DistributionLL;
import org.apache.lucene.search.similarities.IBSimilarity;
import org.apache.lucene.search.similarities.LambdaDF;
import org.apache.lucene.search.similarities.NormalizationH2;

/**
 * Information-based similarity with log-logistic distribution, lambda as average number of documents where w occurs,
 * and normalization 2
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class IBLGDSimilarity extends IBSimilarity {

    /**
     * Creates a new Information-based similarity with log-logistic distribution, lambda as average number of documents
     * where w occurs, and normalization 2
     */
    public IBLGDSimilarity() {
        super(new DistributionLL(), new LambdaDF(), new NormalizationH2());
    }

}
