/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.similarities;

import org.apache.lucene.search.similarities.AfterEffectB;
import org.apache.lucene.search.similarities.BasicModelIn;
import org.apache.lucene.search.similarities.DFRSimilarity;
import org.apache.lucene.search.similarities.NormalizationH2;

/**
 * Divergence From Randomness (DFR) model with Inverse Document Frequency model with Bernoulli after-effect and
 * normalisation 2.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class DFRInB2Similarity extends DFRSimilarity {

    /**
     * Creates a new DFR similarity with Inverse Document Frequency model with Bernoulli after-effect and normalisation
     * 2.
     */
    public DFRInB2Similarity() {
        super(new BasicModelIn(), new AfterEffectB(), new NormalizationH2());
    }

}
