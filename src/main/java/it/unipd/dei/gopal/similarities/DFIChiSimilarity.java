/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.similarities;

import org.apache.lucene.search.similarities.DFISimilarity;
import org.apache.lucene.search.similarities.IndependenceChiSquared;

/**
 * Divergence from Independence (DFI) with normalized chi-squared measure of distance from independence.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class DFIChiSimilarity extends DFISimilarity {

    /**
     * Creates a new Divergence from Independence (DFI) with normalized chi-squared measure of distance from
     * independence.
     */
    public DFIChiSimilarity() {
        super(new IndependenceChiSquared());
    }

}
