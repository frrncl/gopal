/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.similarities;

import org.apache.lucene.search.similarities.*;

/**
 * The data fusion scorer based on CombSUM method using all the other {@code Similarity} in the GoP.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class GopMultiSimilarity extends MultiSimilarity {

    /**
     * The similarities to be merged.
     */
    private static final Similarity[] sims;

    static {
        sims = new Similarity[14];

        // axiomatic models
        sims[0] = new AxiomaticF2EXP();
        sims[1] = new AxiomaticF2LOG();

        // boolean model
        sims[2] = new BooleanSimilarity();

        // BM25 model
        sims[3] = new BM25Similarity();

        // vector space model (lucene version)
        sims[4] = new ClassicSimilarity();

        // Divergence From Independence (DFI) models
        sims[5] = new DFIISSimilarity();
        sims[6] = new DFIChiSimilarity();

        // Divergence From Randomness (DFR) models
        sims[7] = new DFRInB2Similarity();
        sims[8] = new DFRInExpB2Similarity();
        sims[9] = new DFRInL2Similarity();

        // Information-Based models
        sims[10] = new IBLGDSimilarity();
        sims[11] = new IBSPLSimilarity();

        // Language Models
        sims[12] = new LMDirichletSimilarity();
        sims[13] = new LMJelinekMercerSimilarity();
    }


    /**
     * Creates a new data fusion scorer based on the CombSUM method using all the other {@code Similarity} in the GoP..
     */
    public GopMultiSimilarity() {
        super(sims);
    }

}
