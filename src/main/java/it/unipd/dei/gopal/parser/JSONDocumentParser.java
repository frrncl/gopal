/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.parser;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.io.IOException;
import java.io.Reader;

/**
 * The base class for parsing JSON documents.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class JSONDocumentParser extends DocumentParser {

    /**
     * The JSON factory to be used for creating JSON parsers and generators.
     */
    private static final JsonFactory JSON_FACTORY;

    /**
     * The JSON parser for the document.
     */
    protected final JsonParser jp;


    static {
        // the JSON factory
        JSON_FACTORY = new JsonFactory();
        JSON_FACTORY.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
        JSON_FACTORY.disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);

        LOGGER.info("JSON factory factory successfully instantiated.");
    }


    /**
     * Creates a new JSON document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     *
     * @throws NullPointerException  if {@code in} is {@code null}.
     * @throws IllegalStateException if any error occurs while creating the parser.
     */
    protected JSONDocumentParser(final Reader in) {

        super(in);

        try {
            jp = JSON_FACTORY.createParser(in);
        } catch (final IOException e) {
            LOGGER.error("Unable to instantiate the JSON document parser.", e);

            try {
                in.close();
            } catch (IOException ee) {
                LOGGER.error("Unable to close the reader.", ee);
            }

            throw new IllegalStateException("Unable to instantiate the JSON document parser.", e);
        }
    }


    /**
     * Aligns the given JSON parser at the start of the specified field.
     *
     * @param fieldName the name of the field.
     *
     * @return {@code true} if the start of the field {@code fieldName} has been found; {@code false} otherwise.
     *
     * @throws IllegalStateException if something goes wrong while parsing the JSON.
     */
    protected final boolean alignAtElementStart(final String fieldName) throws IllegalStateException {

        if (fieldName == null) {
            LOGGER.error("Field name cannot be null.");
            throw new NullPointerException("Field name cannot be null.");
        }

        if (fieldName.isEmpty()) {
            LOGGER.error("Field name cannot be empty.");
            throw new IllegalStateException("Field name cannot be empty.");
        }

        try {
            // while we are not on the start of an element or the element is not
            // a token element, advance to the next element (if any)
            while (jp.getCurrentToken() != JsonToken.FIELD_NAME || fieldName.equals(jp.getCurrentName()) == false) {

                // there are no more events
                if (jp.nextToken() == null) {
                    return false;
                }
            }
        } catch (final IOException e) {
            final Message msg = new ParameterizedMessage(
                    "Unable to parse JSON and align stream to the start of field {}.", fieldName);
            LOGGER.error(msg, e);
            throw new IllegalStateException(msg.getFormattedMessage(), e);
        }

        return true;
    }
}
