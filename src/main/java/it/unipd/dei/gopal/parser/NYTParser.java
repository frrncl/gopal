/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.parser;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import java.io.FileReader;
import java.io.Reader;

/**
 * Provides a parser for the New York Times Corpus, Linguistic Data Consortium (LDC) catalog number LDC2008T19 and ISBN
 * 1-58563-486-5.
 *
 * See <a href="https://catalog.ldc.upenn.edu/LDC2008T19" target="_new">
 * The New York Times Annotated Corpus</a>.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class NYTParser extends XMLDocumentParser {

    /**
     * The name of the element containing the document identifier.
     */
    private static final String DOCID_ELEMENT = "doc-id";

    /**
     * The name of the attribute containing the document identifier.
     */
    private static final String DOCID_ATTRIBUTE = "id-string";

    /**
     * The name of the element containing the document body.
     */
    private static final String BODY_ELEMENT = "body";

    /**
     * The size of the buffer for the body element.
     */
    private static final int BODY_SIZE = 1024 * 8;


    /**
     * Creates a new New York Times Corpus document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     *
     * @throws NullPointerException     if {@code in} is {@code null}.
     * @throws IllegalArgumentException if any error occurs while creating the parser.
     */
    public NYTParser(final Reader in) {
        super(in);
    }


    @Override
    protected final ParsedDocument parse() {

        final String id;
        final StringBuilder body = new StringBuilder(BODY_SIZE);


        try {

            if (!alignAtElementStart(DOCID_ELEMENT)) {
                Message msg = new ParameterizedMessage("No document identifier {} element found.", DOCID_ELEMENT);
                LOGGER.error(msg);
                throw new IllegalStateException(msg.getFormattedMessage());
            }

            id = xsr.getAttributeValue(null, DOCID_ATTRIBUTE);

            if (!alignAtElementStart(BODY_ELEMENT)) {
                Message msg = new ParameterizedMessage("No document body {} element found.", BODY_ELEMENT);
                LOGGER.error(msg);
                throw new IllegalStateException(msg.getFormattedMessage());
            }

            // parse until we reach the end of the body element
            while (!(xsr.next() == XMLStreamConstants.END_ELEMENT && BODY_ELEMENT.equals(xsr.getLocalName()))) {

                if (xsr.getEventType() == XMLStreamConstants.CHARACTERS && !xsr.isWhiteSpace()) {

                    // Consecutive concatenated elements are separated with a space.
                    // Transform \t \r \n into spaces
                    body.append(StringUtils.replaceChars(
                            StringUtils.replaceChars(StringUtils.replaceChars(xsr.getText(), '\t', ' '), '\r', ' '),
                            '\n', ' ')).append(' ');
                }
            }


        } catch (final XMLStreamException e) {
            LOGGER.error("Unable to parse the XML document.", e);
            throw new IllegalStateException("Unable to parse the XML document.", e);
        } finally {
            next = false;

            try {
                xsr.close();
            } catch (XMLStreamException e) {
                LOGGER.error("Unable to close the XML document parser.", e);
                throw new IllegalStateException("Unable to close the XML document parser.", e);
            }
        }

        return new ParsedDocument(id, body.length() > 0 ? body.toString() : "#");
    }


    public static void main(String[] arg) throws Exception {

        Reader reader = new FileReader(
                "/Users/ferro/Documents/experimental-collections/corpora/TREC/nyt_corpus/documents/01/01/1815847.xml");

        NYTParser p = new NYTParser(reader);

        for (ParsedDocument d : p) {
            System.out.printf("%n%n------------------------------------%n%s%n%n%n", d.toString());
        }


    }

}
