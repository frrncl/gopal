/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.parser;


import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.Reader;

/**
 * The base class for parsing XML documents.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public abstract class XMLDocumentParser extends DocumentParser {


    /**
     * The XML input factory.
     */
    private static final XMLInputFactory XIF;

    /**
     * The XML stream reader for parsing the document.
     */
    protected final XMLStreamReader xsr;


    static {
        // the StAX XML input factory
        XIF = XMLInputFactory.newInstance();

        XIF.setProperty(XMLInputFactory.IS_COALESCING, Boolean.TRUE);
        XIF.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
        XIF.setProperty(XMLInputFactory.IS_VALIDATING, Boolean.FALSE);
        XIF.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.FALSE);
        XIF.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, Boolean.FALSE);

        XIF.setXMLReporter((message, errorType, relatedInformation, location) -> LOGGER
                .error("Error {} at line {}, column {}: {}.", errorType, location.getLineNumber(),
                       location.getColumnNumber(), message));


        LOGGER.info("StAX XML input factory factory successfully instantiated.");
    }


    /**
     * Creates a new XML document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     *
     * @throws NullPointerException  if {@code in} is {@code null}.
     * @throws IllegalStateException if any error occurs while creating the parser.
     */
    protected XMLDocumentParser(final Reader in) {

        super(in);

        try {
            xsr = XIF.createXMLStreamReader(this.in);
        } catch (final XMLStreamException e) {
            LOGGER.error("Unable to instantiate the XML document parser.", e);

            try {
                in.close();
            } catch (IOException ee) {
                LOGGER.error("Unable to close the reader.", ee);
            }

            throw new IllegalStateException("Unable to instantiate the XML document parser.", e);
        }
    }


    /**
     * Aligns the XML stream reader at the start of the specified element.
     *
     * @param localName the name of the element.
     *
     * @return {@code true} if the start of the element {@code localName} has been found; {@code false} otherwise.
     *
     * @throws IllegalStateException if something goes wrong while parsing the XML.
     */
    protected final boolean alignAtElementStart(final String localName) throws IllegalStateException {


        if (localName == null) {
            LOGGER.error("Local name cannot be null.");
            throw new NullPointerException("Local name cannot be null.");
        }

        if (localName.isEmpty()) {
            LOGGER.error("Local name cannot be empty.");
            throw new IllegalStateException("Local name cannot be empty.");
        }


        try {
            // while we are not on the start of an element or the element is not
            // a token element, advance to the next element (if any)
            while (xsr.getEventType() != XMLStreamConstants.START_ELEMENT || !localName.equals(xsr.getLocalName())) {

                // there are no more events
                if (!xsr.hasNext()) {
                    return false;
                }

                xsr.next();
            }
        } catch (final XMLStreamException e) {
            Message msg = new ParameterizedMessage(
                    "Unable to parse the XML document and align stream to the start of element {}.", localName);
            LOGGER.error(msg, e);
            throw new IllegalStateException(msg.getFormattedMessage(), e);
        }

        return true;
    }

    /**
     * Aligns the XML stream reader at the end of the specified element.
     *
     * @param localName the name of the element.
     *
     * @return {@code true} if the end of the element {@code localName} has been found; {@code false} otherwise.
     *
     * @throws IllegalStateException if something goes wrong while parsing the XML.
     */
    protected final boolean alignAtElementEnd(final String localName) throws IllegalStateException {

        if (localName == null) {
            LOGGER.error("Local name cannot be null.");
            throw new NullPointerException("Local name cannot be null.");
        }

        if (localName.isEmpty()) {
            LOGGER.error("Local name cannot be empty.");
            throw new IllegalStateException("Local name cannot be empty.");
        }


        try {

            // while we are not on the end of an element or the element is not
            // a token element, advance to the next element (if any)
            while (xsr.getEventType() != XMLStreamConstants.END_ELEMENT || !localName.equals(xsr.getLocalName())) {

                // there are no more events
                if (!xsr.hasNext()) {
                    return false;
                }

                xsr.next();
            }
        } catch (final XMLStreamException e) {
            Message msg = new ParameterizedMessage(
                    "Unable to parse the XML document and align stream at the end of element {}.", localName);
            LOGGER.error(msg, e);
            throw new IllegalStateException(msg.getFormattedMessage(), e);

        }

        return true;
    }

}
