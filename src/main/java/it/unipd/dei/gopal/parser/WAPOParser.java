/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.parser;

import com.fasterxml.jackson.core.JsonToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.io.*;

/**
 * Provides a parser for the TREC Washington Post Corpus.
 *
 * See <a href="https://trec.nist.gov/data/wapost/" target="_new"> TREC Washington Post Corpus</a>.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class WAPOParser extends DocumentParser {

    /**
     * The currently parsed document
     */
    private String line = null;


    /**
     * Creates a new  Washington Post Corpus document parser.
     *
     * @param in the reader to the document(s) to be parsed.
     *
     * @throws NullPointerException     if {@code in} is {@code null}.
     * @throws IllegalArgumentException if any error occurs while creating the parser.
     */
    public WAPOParser(final Reader in) {
        super(new BufferedReader(in));
    }


    @Override
    public boolean hasNext() {

        try {
            line = ((BufferedReader) in).readLine();

            if (line == null) {
                next = false;
            }
        } catch (IOException e) {
            LOGGER.error("Unable to parse the document.", e);
            throw new IllegalStateException("Unable to parse the document.", e);
        }

        return next;
    }

    @Override
    protected final ParsedDocument parse() {
        return new Parser(line).next();
    }


    public static void main(String[] arg) throws Exception {

        Reader reader = new FileReader(
                "/Users/ferro/Documents/experimental-collections/corpora/TREC/WashingtonPost.v2/data/TREC_Washington_Post_collection.v2.jl");

        WAPOParser p = new WAPOParser(reader);

        for (ParsedDocument d : p) {
            System.out.printf("%n%n------------------------------------%n%s%n%n%n", d.toString());
        }


    }


    /**
     * Provides the actual JSON parser for a single Washington Post document.
     *
     * Since the corpus uses the <a href="http://jsonlines.org/" target="_new">JSON Lines</a> format, where each line is
     * a standalone JSON object representing a document, it is more convenient to mix a simple line reader (the caller
     * of this class) with a full JSON parser for each document (this class).
     *
     * @author Nicola Ferro
     * @version 1.00
     * @since 1.00
     */
    private static class Parser extends JSONDocumentParser {

        /**
         * The name of the field containing the document identifier.
         */
        private static final String DOCID = "id";

        /**
         * The name of the field containing the document body.
         */
        private static final String CONTENTS = "contents";

        /**
         * The name of the field containing a part of the document body.
         */
        private static final String CONTENT = "content";

        /**
         * The name of the field containing the caption of an image.
         */
        private static final String FULL_CAPTION = "fullcaption";

        /**
         * The size of the buffer for the body element.
         */
        private static final int BODY_SIZE = 1024 * 8;


        /**
         * Creates a new Washington Post document parser.
         *
         * @param line the string containing the whole document.
         *
         * @throws NullPointerException     if {@code in} is {@code null}.
         * @throws IllegalArgumentException if any error occurs while creating the parser.
         */
        Parser(final String line) {
            super(new StringReader(line));
        }


        @Override
        protected final ParsedDocument parse() {

            final String id;
            final StringBuilder body = new StringBuilder(BODY_SIZE);

            try {

                if (!alignAtElementStart(DOCID)) {
                    Message msg = new ParameterizedMessage("No document identifier {} element found.", DOCID);
                    LOGGER.error(msg);
                    throw new IllegalStateException(msg.getFormattedMessage());
                }

                jp.nextToken();
                id = jp.getText();

                if (!alignAtElementStart(CONTENTS)) {
                    Message msg = new ParameterizedMessage("No document body {} field found.", CONTENTS);
                    LOGGER.error(msg);
                    throw new IllegalStateException(msg.getFormattedMessage());
                }

                // parse until we reach the end of the body element
                while (jp.nextToken() != JsonToken.END_ARRAY) {

                    if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                        switch (jp.getCurrentName()) {

                            case CONTENT:
                                jp.nextToken();

                                // Consecutive concatenated elements are separated with a space.
                                // Transform \t \r \n,  \u2028, \u2029, \u00A0, \u2007 and \u202F into spaces
                                body.append(StringUtils.replaceChars(StringUtils.replaceChars(StringUtils.replaceChars(
                                        StringUtils.replaceChars(StringUtils.replaceChars(StringUtils.replaceChars(
                                                StringUtils
                                                        .replaceChars(StringUtils.replaceChars(jp.getText(), '\t', ' '),
                                                                      '\r', ' '), '\n', ' '), '\u00A0', ' '), '\u2007',
                                                                 ' '), '\u202F', ' '), '\u2028', ' '), '\u2029', ' '))
                                    .append(' ');
                                break;

                            case FULL_CAPTION:
                                jp.nextToken();

                                // Consecutive concatenated elements are separated with a space.
                                // Transform \t \r \n,  \u2028, \u2029, \u00A0, \u2007 and \u202F into spaces
                                body.append(StringUtils.replaceChars(StringUtils.replaceChars(StringUtils.replaceChars(
                                        StringUtils.replaceChars(StringUtils.replaceChars(StringUtils.replaceChars(
                                                StringUtils
                                                        .replaceChars(StringUtils.replaceChars(jp.getText(), '\t', ' '),
                                                                      '\r', ' '), '\n', ' '), '\u00A0', ' '), '\u2007',
                                                                 ' '), '\u202F', ' '), '\u2028', ' '), '\u2029', ' '))
                                    .append(' ');
                                break;
                        }
                    }
                }
            } catch (final IOException e) {
                LOGGER.error("Unable to parse the JSON document.", e);
                throw new IllegalStateException("Unable to parse the JSON document.", e);
            } finally {
                next = false;

                try {
                    jp.close();
                } catch (IOException e) {
                    LOGGER.error("Unable to close the JSON document parser.", e);
                    throw new IllegalStateException("Unable to close the JSON document parser.", e);
                }
            }

            return new ParsedDocument(id, body.length() > 0 ? body.toString() : "#");
        }
    }

}
