/*
 *  Copyright 2017-2021 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.gopal.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Pre-processes a whole corpus transforming it to a single text file, where each line is a document. It improves bulk
 * indexing.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class PreprocessCorpus {

    /**
     * A LOGGER available for all the subclasses.
     */
    protected static final Logger LOGGER = LogManager.getLogger(PreprocessCorpus.class);

    /**
     * The output file.
     */
    private final BufferedWriter out;

    /**
     * The class of the {@code DocumentParser} to be used.
     */
    private final Class<? extends DocumentParser> dpCls;


    /**
     * The directory (and sub-directories) where documents are stored.
     */
    private final Path docsDir;

    /**
     * The extension of the files to be indexed.
     */
    private final String extension;

    /**
     * The charset used for encoding documents.
     */
    private final Charset cs;

    /**
     * The total number of documents expected to be indexed.
     */
    private final long expectedDocs;

    /**
     * The start instant of the indexing.
     */
    private final long start;

    /**
     * The total number of indexed documents.
     */
    private long docsCount;

    /**
     * The total number of indexed bytes
     */
    private long bytesCount;

    /**
     * Creates a new indexer.
     *
     * @param outputFile   the file where the corpus has to be written.
     * @param docsPath     the directory from which documents have to be read.
     * @param extension    the extension of the files to be indexed.
     * @param charsetName  the name of the charset used for encoding documents.
     * @param expectedDocs the total number of documents expected to be indexed
     * @param dpCls        the class of the {@code DocumentParser} to be used.
     *
     * @throws NullPointerException     if any of the parameters is {@code null}.
     * @throws IllegalArgumentException if any of the parameters assumes invalid values.
     */
    public PreprocessCorpus(final String outputFile, final String docsPath, final String extension,
                            final String charsetName, final long expectedDocs,
                            final Class<? extends DocumentParser> dpCls) {

        if (dpCls == null) {
            LOGGER.error("Document parser class cannot be null.");
            throw new NullPointerException("Document parser class cannot be null.");
        }

        this.dpCls = dpCls;

        if (outputFile == null) {
            LOGGER.error("Output file cannot be null.");
            throw new NullPointerException("Output file cannot be null.");
        }

        if (outputFile.isEmpty()) {
            LOGGER.error("Output file cannot be empty.");
            throw new IllegalArgumentException("Output file cannot be empty.");
        }

        try {
            out = Files.newBufferedWriter(Paths.get(outputFile), StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                                          StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
        } catch (IOException e) {
            Message msg = new ParameterizedMessage("Unable to create output file {}: {}.", outputFile, e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

        if (docsPath == null) {
            LOGGER.error("Documents path cannot be null.");
            throw new NullPointerException("Documents path cannot be null.");
        }

        if (docsPath.isEmpty()) {
            LOGGER.error("Documents path cannot be empty.");
            throw new IllegalArgumentException("Documents path cannot be empty.");
        }

        final Path docsDir = Paths.get(docsPath);
        if (!Files.isReadable(docsDir)) {
            Message msg = new ParameterizedMessage("Documents directory {} cannot be read.",
                                                   docsDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        if (!Files.isDirectory(docsDir)) {
            Message msg = new ParameterizedMessage("{} expected to be a directory of documents.",
                                                   docsDir.toAbsolutePath().toString());
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg.getFormattedMessage());
        }

        this.docsDir = docsDir;

        if (extension == null) {
            LOGGER.error("File extension cannot be null.");
            throw new NullPointerException("File extension cannot be null.");
        }

        if (extension.isEmpty()) {
            LOGGER.error("File extension cannot be empty.");
            throw new IllegalArgumentException("File extension cannot be empty.");
        }
        this.extension = extension;

        if (charsetName == null) {
            LOGGER.error("Charset name cannot be null.");
            throw new NullPointerException("Charset name cannot be null.");
        }

        if (charsetName.isEmpty()) {
            LOGGER.error("Charset name cannot be empty.");
            throw new IllegalArgumentException("Charset name cannot be empty.");
        }

        try {
            cs = Charset.forName(charsetName);
        } catch (Exception e) {
            Message msg = new ParameterizedMessage("Unable to create the charset {}: {}.", charsetName, e.getMessage());
            LOGGER.error(msg, e);
            throw new IllegalArgumentException(msg.getFormattedMessage(), e);
        }

        if (expectedDocs <= 0) {
            LOGGER.error("The expected number of documents to be indexed cannot be less than or equal to zero.");
            throw new IllegalArgumentException(
                    "The expected number of documents to be indexed cannot be less than or equal to zero.");
        }
        this.expectedDocs = expectedDocs;

        this.docsCount = 0;

        this.bytesCount = 0;

        this.start = System.currentTimeMillis();

    }

    /**
     * Indexes a whole directory tree.
     *
     * @throws IOException if something goes wrong while processing.
     */
    public void process() throws IOException {
        Files.walkFileTree(docsDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                if (file.getFileName().toString().endsWith(extension)) {

                    BufferedReader in = Files.newBufferedReader(file, cs);

                    DocumentParser p = DocumentParser.create(dpCls, in);

                    try {
                        for (ParsedDocument d : p) {

                            out.write(d.getIdentifier());

                            out.write('\t');

                            out.write(d.getBody());

                            out.newLine();

                            docsCount++;
                            bytesCount += Files.size(file);

                            if (docsCount % 1000 == 0) {
                                LOGGER.info("{} document(s) ({} bytes) processed in {}.", docsCount, bytesCount,
                                            System.currentTimeMillis() - start);
                            }
                        }
                    } finally {
                        in.close();
                    }


                }
                return FileVisitResult.CONTINUE;
            }
        });

        out.flush();

        out.close();

        if (docsCount != expectedDocs) {
            LOGGER.warn("Expected to index {} documents; {} indexed instead.", expectedDocs, docsCount);
        }

        LOGGER.info("{} document(s) ({} bytes) processed in {}.", docsCount, bytesCount,
                    System.currentTimeMillis() - start);

    }

    public static void main(String[] args) throws Exception {

        //        final String docsPath = "/Users/ferro/Documents/experimental-collections/corpora/TREC/nyt_corpus/documents";
        //        final String outputFile = "/Users/ferro/Documents/pubblicazioni/2019/SIGIR2019/FF/experiment/corpus/nyt.txt";
        //
        //        final String extension = "xml";
        //        final int expectedDocs = 1855658;
        //        final String charsetName = "UTF-8";

        /*final String docsPath = "/Users/ferro/Documents/experimental-collections/corpora/TREC/tipster_trec07";
        final String outputFile = "/Users/ferro/Documents/pubblicazioni/2019/SIGIR2019/FF/experiment/corpus/tipster.txt";

        final String extension = "txt";
        final int expectedDocs = 528155;
        final String charsetName = "ISO-8859-1";*/


        final String docsPath = "/Users/ferro/Documents/experimental-collections/corpora/TREC/WashingtonPost.v2/data";
        final String outputFile = "/Users/ferro/Documents/pubblicazioni/2019/SIGIR2019/FF/gopal/corpus/wapo.txt";

        final String extension = "jl";
        final int expectedDocs = 595037;
        final String charsetName = "UTF-8";

        PreprocessCorpus ppc = new PreprocessCorpus(outputFile, docsPath, extension, charsetName, expectedDocs,
                                                    WAPOParser.class);

        ppc.process();

    }


}
