# GoPAL - Grid of Points for Apache Lucene

This repository contains the source code of _GoPAL - Grid of Points for Apache Lucene_.

It is a driver for creating GoP, i.e. system runs originated by all the possible combinations 
of a set of targeted components, using [Apache Lucene](http://lucene.apache.org/core/).

Developers:

  * Nicola Ferro (`ferro@dei.unipd.it`, [homepage](http://www.dei.unipd.it/~ferro/))
  
    [Department of Information Engineering](http://www.dei.unipd.it/en/)
    
    [University of Padua](http://www.unipd.it/en/), Italy
    
Copyright and license information can be found in the file LICENSE. 
Additional information can be found in the file NOTICE.
